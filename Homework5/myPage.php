<?php
ini_set('memory_limit', '-1');
ini_set('display_errors', 'On');
error_reporting(E_ALL^E_WARNING);
// make sure browsers see this page as utf-8 encoded HTML
header('Content-Type: text/html; charset=utf-8');
header("Access-Control-Allow-Origin: *");

$limit = 10;
$query = isset($_REQUEST['q']) ? $_REQUEST['q'] : false;
$results = false;

if ($query)
{
  // The Apache Solr Client library should be on the include path
  // which is usually most easily accomplished by placing in the
  // same directory as this script ( . or current directory is a default
  // php include path entry in the php.ini)
  require_once('Apache/Solr/Service.php');
  require_once('SpellCorrector.php');

  $arrQuery = explode(' ', $query);
  $arrCorrWords = array();
  $isCorrected = false;

  foreach ($arrQuery as $qword) {
    $arrCorrWords[] = SpellCorrector::correct($qword);    
  }

  if(empty(array_udiff($arrCorrWords, $arrQuery, 'strcasecmp'))) {
    $isCorrected = false;
  } else {
    $isCorrected = true;
  }

  // create a new solr service instance - host, port, and webapp
  // path (all defaults in this example)
  $solr = new Apache_Solr_Service('localhost', 8983, '/solr/irassignment/');

  // if magic quotes is enabled then stripslashes will be needed
  if (get_magic_quotes_gpc() == 1)
  {
    $query = stripslashes($query);
  }

  $searchType = $_GET['searchType'];
  $additionalParameters = array();
  if($searchType == "PageRank_Ranking") {
    $additionalParameters['sort'] = 'pageRankFile desc';
  }
  $additionalParameters['fl'] = 'id, title, description';

  // in production code you'll always want to use a try /catch for any
  // possible exceptions emitted  by searching (i.e. connection
  // problems or a query parsing error)
  try
  {
    //$results = $solr->search($query, 0, $limit);
    $results = $solr->search($query, 0, $limit, $additionalParameters);
  }
  catch (Exception $e)
  {
    // in production you'd probably log or email this error to an admin
    // and then show a special message to the user but for this example
    // we're going to show the full exception
    die("<html><head><title>SEARCH EXCEPTION</title><body><pre>{$e->__toString()}</pre></body></html>");
  }
}

?>
<html>
  <head>
    <title>IR HW 5</title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  </head>
  <body>
    <form  accept-charset="utf-8" method="get">
      <label for="q">Search:</label>
      <input id="q" name="q" type="text" value="<?php echo htmlspecialchars($query, ENT_QUOTES, 'utf-8'); ?>"/><br>
      <input type="radio" name="searchType" value="Default_Ranking" <?php if(isset($_GET['searchType']) && $_GET['searchType'] == 'Default_Ranking') echo 'checked="checked"' ?> > Default Ranking<br>
      <input type="radio" name="searchType" value="PageRank_Ranking" <?php if(isset($_GET['searchType']) && $_GET['searchType'] == 'PageRank_Ranking') echo 'checked="checked"' ?> > PageRank Ranking<br>
      <input type="submit"/>
    </form>
<?php

// display results
if ($results)
{
  $mapData = array();
  $file = fopen('mapLATimesDataFile.csv', 'r');
  while(($row = fgetcsv($file)) !== FALSE) {
    $mapData[$row[0]] = $row[1];
  }
  $total = (int) $results->response->numFound;
  $start = min(1, $total);
  $end = min($limit, $total);
  
  if($isCorrected == true) {
  $completeQuery = "";
  foreach($arrCorrWords as $values) {
    $completeQuery = $completeQuery.$values." ";
  }
  $completeQuery = trim($completeQuery);
  $completeLink = $completeQuery;
  $searchType = $_GET['searchType'];
  if($searchType == 'PageRank_Ranking') {
    $completeLink = $completeLink."&searchType=PageRank_Ranking";
  } else {
    $completeLink = $completeLink."&searchType=Default_Ranking";
  }
?>
    <p>Did you mean: <a href="http://localhost/myPage.php?q=<?php echo $completeLink; ?>"><?php echo $completeQuery; ?></a></p>
<?php
}
?>
    <div>Results <?php echo $start; ?> - <?php echo $end;?> of <?php echo $total; ?>:</div>
    <table style="width: 100%; text-align: left; border-spacing: 10px;">
<?php
  // iterate result documents
  $counter = 1;
  foreach ($results->response->docs as $doc)
  {
    $id = $doc->id;
  //Locate the document id
  $fileName = basename($id);
  $fileName = basename($fileName, ".html");
  $fileName = $fileName.".txt";
  $snippet = "";
  $queryArr = explode(" ",$query);
  $handle = fopen("/home/dreamy/Documents/IR/snippets/".$fileName, "r") or die("Unable to open file!");
  $flag = 0;
  $finalSnippet = "";

  
  while (($line = fgets($handle)) !== false) {
        foreach($queryArr as $item){

      if (stripos($line, $item) !== false) {
        $index = stripos($line,$item);
        $flag = 1;
        
        if($index > 152){
          $start = $index - 30;
          $end = $index + min(strlen($line),30);
          
          $snippet = substr($line,$start,$end-$start);
          $snippet = $snippet."...";
          $snippet = str_ireplace($item, "<strong>".$item."</strong>", $snippet);
          $finalSnippet = $finalSnippet.$snippet;
          
        }else{
          $snippet = $line;
          
          $snippet = str_ireplace($item, "<strong>".$item."</strong>", $snippet);
          $finalSnippet = $finalSnippet.$snippet;
          
        }
      }   
    }
    
    if($flag == 1){
      break;
    }
    
    }
  fclose($handle);
?>
      <tr>
        <td>
          <?php echo htmlspecialchars($counter . ".", ENT_NOQUOTES, 'utf-8'); $counter++?>
        </td>
        <td>
          <a href = "<?php echo htmlspecialchars($mapData[basename($doc->id)], ENT_NOQUOTES, 'utf-8'); ?>" target = "_blank"> <?php echo "Title: "; echo htmlspecialchars($doc->title, ENT_NOQUOTES, 'utf-8'); ?> </a> <br>
          <a href = "<?php echo htmlspecialchars($mapData[basename($doc->id)], ENT_NOQUOTES, 'utf-8'); ?>" target = "_blank"> <?php echo "URL: "; echo htmlspecialchars(trim($mapData[basename($doc->id)]), ENT_NOQUOTES, 'utf-8'); ?> </a><br>
          <p> <?php echo $finalSnippet ?> </p> <br>
          <?php echo "ID: "; echo htmlspecialchars($doc->id, ENT_NOQUOTES, 'utf-8'); ?><br>
          <?php echo "Descripiton: "; if($doc->description) echo htmlspecialchars($doc->description, ENT_NOQUOTES, 'utf-8');
                else echo "N/A" ?>
        </td>
      </tr>
<?php
  }
?>
    </table>
<?php
}
?>
  <script>    
        $(function() {
      $("#q").autocomplete({
          minLength: 1,
          source : function(request, response) {
            var last = request.term.toLowerCase().trim().split(" ").pop(-1);            
            $.ajax({
              url : "http://localhost:8983/solr/irassignment/suggest?q=" + last + "&wt=json",
              success : function (data,textStatus, jqXHR) {
              var suggestions = data.suggest.suggest[last].suggestions;
                        
              suggestions = $.map(suggestions, function (value, key) {
                var next = "";
                var query = $("#q").val();
                var queries = query.split(" ");
                
                if (queries.length > 1) {
                  var lastIndex = query.lastIndexOf(" ");
                  next = query.substring(0, lastIndex + 1).toLowerCase();
                }
                
                if (!/^[0-9a-zA-Z]+$/.test(value.term)) {
                  return null;
                }
                return next + value.term;
              });
              response(suggestions.slice(0, 10));
            },
              dataType : 'jsonp',
              jsonp : 'json.wrf'
            });
          },
        });
        });
        
    </script>
  </body>
</html>