import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;


public class InvertedIndex {

	public static void main(String[] args) 
		throws IOException, ClassNotFoundException, InterruptedException{
		
		if(args.length != 2) {
			System.err.println("Usage: InvertedIndex <input_path> <output_path>");
			System.exit(-1);
		}
		JobConf conf = new JobConf(InvertedIndex.class);
		conf.setJobName("Inverted Index");
		
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		
		conf.setMapperClass(InvertedIndexMapper.class);
		conf.setReducerClass(InvertedIndexReducer.class);
		
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);
		
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));
		
		JobClient.runJob(conf);
		
	}

}

class InvertedIndexMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

	@Override
	public void map(LongWritable key, Text value,
			OutputCollector<Text, Text> collector, Reporter reporter) throws IOException {
		Text docID = null;
		boolean firstTime = true;
		StringTokenizer tokenizer = new StringTokenizer(value.toString());
		while(tokenizer.hasMoreTokens()) {
			if(firstTime){
				firstTime = false;
				docID = new Text(tokenizer.nextToken());
				continue;
			}
			Text word = new Text();
			word.set(tokenizer.nextToken().trim());
			collector.collect(word, docID);
		}
	}	
}

class InvertedIndexReducer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

	@Override
	public void reduce(Text key, Iterator<Text> values,
			OutputCollector<Text, Text> collector, Reporter reporter) throws IOException {
		Map<String, Integer> dict = new HashMap<>();
		while(values.hasNext()) {
			Text value = values.next();
			if(dict.containsKey(value.toString())) {
				dict.put(value.toString(), dict.get(value.toString())+1);
			} else {
				dict.put(value.toString(), 1);
			}
		}
		
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String, Integer> entry : dict.entrySet()) {
			sb.append(entry.getKey().toString());
			sb.append(':');
			sb.append(entry.getValue());
			sb.append('\t');
		}
		collector.collect(key, new Text(sb.toString()));
	}	
}