import networkx as nx
import os as os
htmlPath = "/home/dreamy/Documents/IR/solr-6.5.0/crawl_data/"
outputFile = open("/home/dreamy/Documents/IR/external_pageRankFileJava.txt", 'w+')
graph = nx.read_edgelist("edgeList_java.txt", create_using=nx.DiGraph())
rank = nx.pagerank(graph, alpha=0.85, personalization=None, max_iter=30, tol=1e-06, nstart=None, weight='weight', dangling=None)
for key in rank.viewkeys():
	outputFile.write((os.path.join(htmlPath, key)) + '=' + str(rank[key]) + '\n')