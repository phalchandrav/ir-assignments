from bs4 import BeautifulSoup as bs
import os as os
import csv as csv
import urlparse as urlparse
import codecs as codecs

def is_url_absolute(url):
	return bool(urlparse.urlparse(url).netloc)

htmlDirPath = "/home/dreamy/Documents/IR/solr-6.5.0/crawl_data/"
mapFilePath = "/home/dreamy/Documents/IR/mapLATimesDataFile.csv"
outputFile = "/home/dreamy/Documents/IR/edgeList.txt"
outFile = open(outputFile, 'w+')
fileToUrl = dict()
urlToFile = dict()
with open(mapFilePath, 'rb') as csvfile:
	for row in csv.reader(csvfile, delimiter=',', quoting=csv.QUOTE_NONE):
		fileToUrl[row[0]] = row[1]
		urlToFile[row[1]] = row[0]

counter = 0
edges = set()
for file in os.listdir(htmlDirPath):
	filePath = os.path.join(htmlDirPath, file)
	data = codecs.open(filePath, "r", "utf-8").read()
	soup = bs(data, 'html.parser')
	for link in soup.find_all('a', href=True):
		actLink = link.get('href').strip()
		if(not is_url_absolute(actLink)):
			actLink = urlparse.urljoin(fileToUrl[file], actLink).strip()

		if(actLink in urlToFile):
			edges.add(file + ' ' + urlToFile[actLink] + '\n')
	
	counter+=1
	if((counter%25) == 0):
		print str(counter) + '\n'

for str in edges:
	outFile.write(str)