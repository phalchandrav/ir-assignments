import sys
import re
from os import listdir
from os.path import isfile, join
import tika
from tika import parser
onlyfiles = [f for f in listdir("/home/dreamy/Documents/IR/solr-6.5.0/crawl_data") if isfile(join("/home/dreamy/Documents/IR/solr-6.5.0/crawl_data", f))]

def main():
	wordsSet = set()
	outputFile = open('big3.txt', 'w')
	counter = 0
	for of in onlyfiles:
		data = parser.from_file("/home/dreamy/Documents/IR/solr-6.5.0/crawl_data/" + str(of))
		if(data['content'] != None):
			dataStrs = data['content'].split()
			for word in dataStrs:
				newWord = word.encode('utf-8')
				wordsSet.add(newWord)
		counter += 1
		if (counter % 50 == 0):
			print counter

	for word in wordsSet:
		outputFile.write(word + ' ')

if __name__ == '__main__':
	main()