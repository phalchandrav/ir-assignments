import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ExtractLinks {

	public static void main(String[] args) throws Exception{
		Map<String, String> fileToUrlMap = new HashMap<>();
		Map<String, String> urlToFileMap = new HashMap<>();
		Set<String> edges = new HashSet<>();
		File csvFile = new File("/home/dreamy/Documents/IR/mapLATimesDataFile.csv");
		BufferedReader br = new BufferedReader(new FileReader(csvFile));
		String line;
		while((line = br.readLine()) != null) {
			String tokens[] = line.split(",");
			fileToUrlMap.put(tokens[0], tokens[1]);
			urlToFileMap.put(tokens[1], tokens[0]);
		}
		br.close();
		PrintWriter writer = new PrintWriter("/home/dreamy/Documents/IR/edgeList_java.txt", "UTF-8");
		File htmlDirectory = new File("/home/dreamy/Documents/IR/solr-6.5.0/crawl_data");
		if(htmlDirectory.isDirectory()) {
			int counter = 0;
			for(File file : htmlDirectory.listFiles()) {
				Document doc = Jsoup.parse(file, "UTF-8", fileToUrlMap.get(file.getName()));
				Elements links = doc.select("a[href]");
				for(Element link : links) {
					String absUrl = link.attr("abs:href").trim();
					if(urlToFileMap.containsKey(absUrl)) {
						edges.add(file.getName() + " " + urlToFileMap.get(absUrl));
					}
				}
				counter++;
				if(counter%50 == 0)
					System.out.println(counter);
			}
			for(String str : edges) {
				writer.println(str);
			}
			writer.flush();
			writer.close();
		}
	}

}
